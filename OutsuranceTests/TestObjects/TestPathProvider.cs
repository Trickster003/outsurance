﻿using Outsurance.Interface;
using System;
using System.IO;
using System.Linq;

namespace OutsuranceTests.TestObjects
{
  public class TestPathProvider : IPathProvider
  {
    public string MapPath ( string path )
    {
      string startPath = AppDomain.CurrentDomain.BaseDirectory;
      var pathItems = startPath.Split(Path.DirectorySeparatorChar);
      string projPath = String.Join(Path.DirectorySeparatorChar.ToString(), pathItems.Take(pathItems.Length - 2));
      return Path.Combine(projPath, "\\output\\data\\");
    }
  }
}
