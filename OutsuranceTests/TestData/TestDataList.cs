﻿using Outsurance.Model;
using System.Collections.Generic;

namespace OutsuranceTests.TestData
{
  public class TestDataList
  {
    public static string[] ExpectedProcessedAddressList ()
    {
      return new string[]
      {
        "65 Ambling Way",
        "8 Crimson Rd",
        "12 Howard St",
        "102 Long Lane",
        "94 Roland St",
        "78 Short Lane",
        "82 Stewart St",
        "49 Sutherland St"
      };
    }
    public static string[] ExpectedProcessedNameList ()
    {
      return new string[]
      {
        "Brown, 2",
        "Clive, 2",
        "Graham, 2",
        "Howe, 2",
        "James, 2",
        "Owen, 2",
        "Smith, 2",
        "Jimmy, 1",
        "John, 1"
      };
    }
    public static List<ProfileModel> ExpectedParsedCsvData ()
    {
      List<ProfileModel> expectedResult = new List<ProfileModel>();
      Dictionary<string, string> items = new Dictionary<string, string>();
      items.Add("FirstName", "Jimmy");
      items.Add("LastName", "Smith");
      items.Add("Address", "102 Long Lane");
      items.Add("PhoneNumber", "29384857");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();

      items.Add("FirstName", "Clive");
      items.Add("LastName", "Owen");
      items.Add("Address", "65 Ambling Way");
      items.Add("PhoneNumber", "31214788");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();

      items.Add("FirstName", "James");
      items.Add("LastName", "Brown");
      items.Add("Address", "82 Stewart St");
      items.Add("PhoneNumber", "32114566");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();

      items.Add("FirstName", "Graham");
      items.Add("LastName", "Howe");
      items.Add("Address", "12 Howard St");
      items.Add("PhoneNumber", "8766556");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();

      items.Add("FirstName", "John");
      items.Add("LastName", "Howe");
      items.Add("Address", "78 Short Lane");
      items.Add("PhoneNumber", "29384857");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();

      items.Add("FirstName", "Clive");
      items.Add("LastName", "Smith");
      items.Add("Address", "49 Sutherland St");
      items.Add("PhoneNumber", "31214788");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();

      items.Add("FirstName", "James");
      items.Add("LastName", "Owen");
      items.Add("Address", "8 Crimson Rd");
      items.Add("PhoneNumber", "32114566");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();


      items.Add("FirstName", "Graham");
      items.Add("LastName", "Brown");
      items.Add("Address", "94 Roland St");
      items.Add("PhoneNumber", "8766556");
      expectedResult.Add(new ProfileModel(items));
      items.Clear();
      return expectedResult;

    }

  }
}
