﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.Service;
using OutsuranceTests.TestData;
using System;
using System.IO;
using System.Linq;

namespace OutsuranceTests
{
  /// <summary>
  /// Summary description for UnitTest1
  /// </summary>
  [TestClass]
  public class WriteToDiskTest
  {
    public WriteToDiskTest ()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void WriteDataToDiskTest ()
    {
      var NAME_REPORT = "NameReport.txt";
      var ADDRESS_REPORT = "AddressReport.txt";
      string startPath = AppDomain.CurrentDomain.BaseDirectory;
      var pathItems = startPath.Split(Path.DirectorySeparatorChar);
      string projPath = String.Join(Path.DirectorySeparatorChar.ToString(), pathItems.Take(pathItems.Length - 2));
      var outputPath = Path.Combine(projPath, "output\\data\\");

      PersistToDiskService service = new PersistToDiskService();

      service.WriteToTextFile(TestDataList.ExpectedProcessedNameList(), outputPath, NAME_REPORT);

      var locationExists = Directory.Exists(outputPath);

      var fileExists = File.Exists(outputPath + NAME_REPORT);

      var file = File.OpenRead(outputPath + NAME_REPORT);
      var lineCount = File.ReadLines(outputPath + NAME_REPORT).Count();

      Assert.IsNotNull(file);
      Assert.AreNotEqual(0, file.Length);
      Assert.AreEqual(TestDataList.ExpectedProcessedNameList().Length, lineCount);
      Assert.IsTrue(locationExists);
      Assert.IsTrue(fileExists);

      service.WriteToTextFile(TestDataList.ExpectedProcessedAddressList(), outputPath, ADDRESS_REPORT);

      locationExists = Directory.Exists(outputPath);

      fileExists = File.Exists(outputPath + ADDRESS_REPORT);

      file = File.OpenRead(outputPath + ADDRESS_REPORT);
      lineCount = File.ReadLines(outputPath + ADDRESS_REPORT).Count();

      Assert.IsNotNull(file);
      Assert.AreNotEqual(0, file.Length);
      Assert.AreEqual(TestDataList.ExpectedProcessedAddressList().Length, lineCount);
      Assert.IsTrue(locationExists);
      Assert.IsTrue(fileExists);



    }
  }
}
