﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.Controllers;
using Outsurance.Model;
using Outsurance.Service;
using OutsuranceTests.TestData;
using OutsuranceTests.TestObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OutsuranceTests
{
  /// <summary>
  /// Summary description for HomeControllerTest
  /// </summary>
  [TestClass]
  public class HomeControllerTest
  {
    public HomeControllerTest ()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void ProcessCsvFileControllerTest ()
    {
      //
      string startPath = System.AppDomain.CurrentDomain.BaseDirectory;
      var pathItems = startPath.Split(Path.DirectorySeparatorChar);
      string projPath = String.Join(Path.DirectorySeparatorChar.ToString(), pathItems.Take(pathItems.Length - 2));
      var pathProvider = new TestPathProvider();

      var dataPath = Path.Combine(projPath, "TestData", "data.csv");
      FileStream test = File.OpenRead(dataPath);

      HomeController controller = new HomeController(new TestPathProvider());
      ViewResult result = (ViewResult)controller.ProcessCsvFile(new TestHttpPostedFileBase(test, MimeMapping.GetMimeMapping(dataPath), "data.csv"));

      Assert.IsNotNull(result);
      Assert.IsInstanceOfType(result, typeof(ViewResult));
      Assert.AreEqual(result.ViewName, "ProcessCsvFile");
    }

    [TestMethod]
    public void TestCsvParser ()
    {
      string startPath = System.AppDomain.CurrentDomain.BaseDirectory;
      var pathItems = startPath.Split(Path.DirectorySeparatorChar);
      string projPath = String.Join(Path.DirectorySeparatorChar.ToString(), pathItems.Take(pathItems.Length - 2));
      var dataPath = Path.Combine(projPath, "TestData", "data.csv");
      FileStream test = File.OpenRead(dataPath);

      List<ProfileModel> expectedResult = TestDataList.ExpectedParsedCsvData();

      List<ProfileModel> parsedList = CSVParser.Parse<ProfileModel>(test).ToList();
      Assert.IsNotNull(parsedList);
      CollectionAssert.AllItemsAreNotNull(parsedList);
      CollectionAssert.AllItemsAreUnique(parsedList);
      CollectionAssert.AllItemsAreInstancesOfType(parsedList, typeof(ProfileModel));
      CollectionAssert.AreEqual(expectedResult, parsedList, new ParsedListComparer());


    }

    private class ParsedListComparer : Comparer<ProfileModel>
    {
      public override int Compare ( ProfileModel expected, ProfileModel parsed )
      {
        var type = typeof(ProfileModel);
        var query = from expectedProp in type.GetProperties()
                    let expectedVal = expectedProp.GetValue(expected)
                    let parsedProp = type.GetProperty(expectedProp.Name)
                    let parsedVal = parsedProp.GetValue(parsed)
                    where !expectedVal.Equals(parsedVal)
                    select new { expectedProp.Name, expectedVal, parsedVal };

        return query.Any() ? -1 : 0;

      }
    }

  }
}
