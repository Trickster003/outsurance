﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.Service;
using Outsurance.Strategy.Reporting;
using OutsuranceTests.TestData;

namespace OutsuranceTests
{
  /// <summary>
  /// Summary description for UnitTest2
  /// </summary>
  [TestClass]
  public class ReportingServiceTest
  {
    public ReportingServiceTest ()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void Sort ()
    {
      ReportingService service = new ReportingService();

      var result = service.Sort<AddressReport>(TestDataList.ExpectedParsedCsvData());
      Assert.IsNotNull(result);

      CollectionAssert.AreEqual(TestDataList.ExpectedProcessedAddressList(), result);
      CollectionAssert.AllItemsAreNotNull(result);
      CollectionAssert.AllItemsAreInstancesOfType(result, typeof(string));

      result = service.Sort<AddressReport>(null);

      Assert.IsNull(result);


      result = service.Sort<ProfileNameReport>(TestDataList.ExpectedParsedCsvData());
      Assert.IsNotNull(result);

      CollectionAssert.AreEqual(TestDataList.ExpectedProcessedNameList(), result);
      CollectionAssert.AllItemsAreNotNull(result);
      CollectionAssert.AllItemsAreInstancesOfType(result, typeof(string));

      result = service.Sort<ProfileNameReport>(null);

      Assert.IsNull(result);
    }
  }
}
