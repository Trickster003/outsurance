﻿namespace Outsurance.Interface
{
  public interface IPathProvider
  {
    string MapPath ( string path );
  }
}
