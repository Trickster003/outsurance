﻿using Outsurance.Model;
using System.Collections.Generic;

namespace Outsurance.Interface
{
  public interface IReportStrategy
  {
    string[] Execute ( IEnumerable<ProfileModel> list );
  }
}
