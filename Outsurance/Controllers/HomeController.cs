﻿using Outsurance.Interface;
using Outsurance.Model;
using Outsurance.Provider;
using Outsurance.Service;
using Outsurance.Strategy.Reporting;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Outsurance.Controllers
{
  public class HomeController : Controller
  {

    private readonly IPathProvider _pathProvider;
    public HomeController () : this(new ServerPathProvider())
    {

    }

    public HomeController ( IPathProvider pathProvider )
    {
      _pathProvider = pathProvider;
    }
    private const string REPORT_PATH = "/output/data/";
    public ActionResult Index ()
    {
      return View();
    }

    public ActionResult ProcessCsvFile ( HttpPostedFileBase file )
    {
      if (file != null && file.ContentLength > 0)
      {
        if (file.FileName.EndsWith(".csv"))
        {
          IEnumerable<ProfileModel> csvObjects = CSVParser.Parse<ProfileModel>(file.InputStream);
          if (csvObjects == null)
          {
            ModelState.AddModelError("", "Error Parsing the csvObjects");
            return View("Index", ModelState);
          }
          var sortService = new ReportingService();
          var writeToDiskService = new PersistToDiskService(); ;

          try
          {
            var result = sortService.Sort<ProfileNameReport>(csvObjects);
            writeToDiskService.WriteToTextFile(result, _pathProvider.MapPath(REPORT_PATH), "NameReport.txt");

          }
          catch (Exception e)
          {
            ModelState.AddModelError("", "Error running the Profile Report");
            return View("Error", ModelState);
          }

          try
          {
            var result = sortService.Sort<AddressReport>(csvObjects);
            writeToDiskService.WriteToTextFile(result, _pathProvider.MapPath(REPORT_PATH), "AddressReport.txt");
          }
          catch (Exception e)
          {
            ModelState.AddModelError("", "Error running the Address Report");
            return View("Error", ModelState);
          }
        }
        else
        {
          ModelState.AddModelError("", "This file format is not supported, .csv file required");
          return View("Index", ModelState);
        }
      }
      return View("ProcessCsvFile");
    }
  }
}