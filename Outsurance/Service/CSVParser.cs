﻿using Outsurance.Abstract;
using System;
using System.Collections.Generic;
using System.IO;

namespace Outsurance.Service
{
  public class CSVParser
  {
    public static IEnumerable<T> Parse<T> ( Stream file ) where T : CsvObject
    {
      var list = new List<T>();
      var dict = new Dictionary<string, string>();
      try
      {
        using (StreamReader csvReader = new StreamReader(file))
        {

          var headers = csvReader.ReadLine().Split(',');
          while (!csvReader.EndOfStream)
          {
            var line = csvReader.ReadLine();
            var values = line.Split(',');
            for (int i = 0; i < headers.Length; i++)
            {
              dict.Add(headers[i], values[i]);
            }
            var csvObj = (T)Activator.CreateInstance(typeof(T), dict);
            list.Add(csvObj);
            dict.Clear();
          }
        }
      }
      catch (Exception e)
      {
        throw e;
      }


      return list;
    }
  }
}