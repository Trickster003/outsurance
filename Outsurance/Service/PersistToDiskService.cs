﻿using System;
using System.IO;

namespace Outsurance.Service
{
  public class PersistToDiskService
  {
    public void WriteToTextFile ( string[] data, string path, string filename )
    {
      if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(filename))
      {
        if (!Directory.Exists(path))
        {
          Directory.CreateDirectory(path);
        }
        try
        {
          File.WriteAllLines(path + filename, data);
        }
        catch (Exception e)
        {
          throw e;
        }
      }
      else
      {
        throw new Exception("path " + path + " or filename " + filename + " parameter cannot be null or empty");
      }

    }
  }
}