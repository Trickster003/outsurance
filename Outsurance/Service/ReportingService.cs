﻿using Outsurance.Interface;
using Outsurance.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Outsurance.Service
{
  public class ReportingService
  {
    public string[] Sort<A> ( IEnumerable<ProfileModel> list ) where A : IReportStrategy
    {
      if (list != null && list.Any())
      {
        var sortStrategy = (A)Activator.CreateInstance(typeof(A));

        return sortStrategy.Execute(list);
      }
      return null;
    }
  }
}