﻿using System;
using System.Collections.Generic;

namespace Outsurance.Abstract
{
  public abstract class CsvObject
  {
    protected CsvObject(Dictionary<string, string> data)
    {
      Type type = GetType();
      foreach (string key in data.Keys)
      {
        var value = data[key];
        var propInfo = type.GetProperty(key);
        propInfo.SetValue(this, value, null);
      }
    }
    public abstract Type GetType();
  }
}