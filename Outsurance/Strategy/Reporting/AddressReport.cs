﻿using Outsurance.Interface;
using Outsurance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Outsurance.Strategy.Reporting
{
  public class AddressReport : IReportStrategy
  {
    public string[] Execute ( IEnumerable<ProfileModel> list )
    {
      if (list != null && list.Any())
      {
        var result = list.OrderBy(x => IgnoreNumbers(x.Address)).ToList();
        return result.Select(i => i.Address).ToArray();
        //if (result.Any())
        //{
        //  WriteToTextFile(result.Select(i => i.Address).ToArray(), "/output/data/", "address.txt");
        //}
      }
      return null;
    }

    private string IgnoreNumbers ( string str )
    {
      StringBuilder stringBuilder = new StringBuilder();
      foreach (Char c in str)
      {
        if (!Char.IsDigit(c))
        {
          stringBuilder.Append(c);
        }


      }
      return stringBuilder.ToString();
    }
  }
}