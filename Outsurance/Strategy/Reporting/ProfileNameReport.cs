﻿using Outsurance.Interface;
using Outsurance.Model;
using System.Collections.Generic;
using System.Linq;

namespace Outsurance.Strategy.Reporting
{
  public class ProfileNameReport : IReportStrategy
  {
    public string[] Execute ( IEnumerable<ProfileModel> list )
    {
      if (list != null && list.Any())
      {
        return CountFirstNameOccurences(list);
      }

      return null;
    }

    private string[] CountFirstNameOccurences ( IEnumerable<ProfileModel> list )
    {

      //var first = list.GroupBy(i => i.FirstName).Then

      var firstName =
        list.GroupBy(i => i.FirstName)
          .Select(group => new NameResults(group.Key, group.Count()));

      var results = firstName.Concat(list.GroupBy(i => i.LastName)
                      .Select(group => new NameResults(group.Key, group.Count())));


      results = results.OrderByDescending(i => i.Count).ThenBy(i => i.Name);

      return results.Select(i => i.ToString()).ToArray();
      //if (results.Any())
      //{
      //  WriteToTextFile(results.Select(i => i.ToString()).ToArray(), "/output/data/", "names.txt");
      //}


    }
  }

  public class NameResults
  {

    public string Name { get; set; }
    public int Count { get; set; }

    public NameResults ( string name, int count )
    {
      Name = name;
      Count = count;
    }

    public override string ToString ()
    {
      return Name + ", " + Count.ToString();
    }
  }
}