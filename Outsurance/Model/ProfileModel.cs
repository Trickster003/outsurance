﻿using Outsurance.Abstract;
using System;
using System.Collections.Generic;

namespace Outsurance.Model
{
  public class ProfileModel : CsvObject
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Address { get; set; }
    public string PhoneNumber { get; set; }

    public ProfileModel ( Dictionary<string, string> data ) : base(data)
    {

    }

    public override Type GetType ()
    {
      return typeof(ProfileModel);
    }
  }
}