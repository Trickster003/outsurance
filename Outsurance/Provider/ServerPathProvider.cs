﻿using Outsurance.Interface;
using System.Web;

namespace Outsurance.Provider
{
  public class ServerPathProvider : IPathProvider
  {
    public string MapPath ( string path )
    {
      return HttpContext.Current.Server.MapPath(path);
    }
  }
}